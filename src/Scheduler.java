import java.util.Scanner;

public class Scheduler {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("enter no of process: ");

        int inputNumber = scanner.nextInt();
        int processIds[] = new int[inputNumber];
        int arrivalTimes[] = new int[inputNumber];
        int timeExecutions[] = new int[inputNumber];
        int timeCompletions[] = new int[inputNumber];
        int turnAroundTimes[] = new int[inputNumber];
        int waitingTimes[] = new int[inputNumber];
        int temp;

        for (int i = 0; i < inputNumber; i++) {
            System.out.println("enter process " + (i + 1) + " arrival time: ");
            arrivalTimes[i] = scanner.nextInt();
            System.out.println("enter process " + (i + 1) + " execution time: ");
            timeExecutions[i] = scanner.nextInt();
            processIds[i] = i + 1;
        }

        //sorting according to arrival times
        for (int i = 0; i < inputNumber; i++) {
            for (int j = 0; j < inputNumber - (i + 1); j++) {
                if (arrivalTimes[j] > arrivalTimes[j + 1]) {
                    temp = arrivalTimes[j];
                    arrivalTimes[j] = arrivalTimes[j + 1];
                    arrivalTimes[j + 1] = temp;
                    temp = timeExecutions[j];
                    timeExecutions[j] = timeExecutions[j + 1];
                    timeExecutions[j + 1] = temp;
                    temp = processIds[j];
                    processIds[j] = processIds[j + 1];
                    processIds[j + 1] = temp;
                }
            }
        }

        // finding completion times
        float averageWaitingTime = 0;
        float averageTurnAroundTime = 0;

        for (int i = 0; i < inputNumber; i++) {
            if (i == 0) {
                timeCompletions[i] = arrivalTimes[i] + timeExecutions[i];
            } else {
                if (arrivalTimes[i] > timeCompletions[i - 1]) {
                    timeCompletions[i] = arrivalTimes[i] + timeExecutions[i];
                } else
                    timeCompletions[i] = timeCompletions[i - 1] + timeExecutions[i];
            }
            turnAroundTimes[i] = timeCompletions[i] - arrivalTimes[i]; // turnaround time= completion time- arrival time
            waitingTimes[i] = turnAroundTimes[i] - timeExecutions[i];  // waiting time= turnaround time- burst time
            averageWaitingTime += waitingTimes[i];                     // total waiting time
            averageTurnAroundTime += turnAroundTimes[i];               // total turnaround time
        }

        System.out.println("\npid,arrival,execution,complete,turn,waiting");

        for (int i = 0; i < inputNumber; i++) {
            System.out.println(processIds[i] + "," + arrivalTimes[i] + "," + timeExecutions[i] + "," + timeCompletions[i] + "," + turnAroundTimes[i] + "," + waitingTimes[i]);
        }
        scanner.close();

        System.out.println("\naverage waiting time: " + (averageWaitingTime / inputNumber));
        System.out.println("average turnaround time:" + (averageTurnAroundTime / inputNumber));
    }
}
